<?php namespace EveraldoReis\Posts\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreatePostsTable extends Migration {

	public function up() {
		Schema::create('everaldoreis_posts_posts', function ($table) {
			$table->engine = 'MyISAM';
			$table->increments('id');
			$table->integer('categoria_id')->reference('id')
			->on('everaldoreis_posts_categorias')->nullable(true);
			$table->string('name')->nullable(false);
			$table->string('slug')->nullable(false);
			$table->text('description')->nullable(true);
			$table->boolean('published')->default(1)->nullable();
			$table->integer('sort_order')->default(0)->nullable();
			$table->integer('nest_left')->nullable();
			$table->integer('nest_right')->nullable();
			$table->integer('nest_depth')->nullable();
			$table->timestamps();
		});
	}

	public function down() {
		Schema::dropIfExists('everaldoreis_posts_posts');
	}

}
