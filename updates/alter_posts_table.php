<?php namespace EveraldoReis\Posts\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AlterPostsTable extends Migration {

	public function up() {

		if (!Schema::hasColumn('everaldoreis_posts_posts', 'show_link')) {
			Schema::table('everaldoreis_posts_posts', function ($table) {
				$table->boolean('show_link')->default(1)->nullable();
			});
		}

		Schema::table('everaldoreis_posts_posts', function ($table) {
			$table->boolean('show_link')->default(1)->nullable();
		});
	}

	public function down() {
		Schema::table('everaldoreis_posts_posts', function ($table) {
			$table->dropColumn('show_link');
		});
	}

}
