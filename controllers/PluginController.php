<?php namespace EveraldoReis\Posts\Controllers;
use BackendMenu;
use Backend\Classes\Controller;
use EveraldoReis\Posts\Models\Categoria;
use Flash;
use Str;

class PluginController extends Controller {

	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController',
		'Backend.Behaviors.RelationController',
	];

	public $sortable = false;

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';
	public $relationConfig = 'config_relation.yaml';

	public function __construct() {
		parent::__construct();

		$class = explode('\\', get_class($this));
		$context = array_pop($class);
		array_pop($class);
		$namespace = array_pop($class);

		BackendMenu::setContext('EveraldoReis.' . $namespace, strtolower($namespace), strtolower($context));
		if ($this->sortable) {
			// Grab the drag and drop requirements
			$this->addCss('/plugins/everaldoreis/' . strtolower($namespace) . '/assets/css/sortable.css');
			$this->addJs('/plugins/everaldoreis/' . strtolower($namespace) . '/assets/js/html5sortable.js');
			$this->addJs('/plugins/everaldoreis/' . strtolower($namespace) . '/assets/js/sortable.js');
		}
	}

	/**
	 * Extend the list query to position the rows correctly
	 */
	public function listExtendQuery($query, $definition = null) {
		if (!$this->user->isSuperUser()) {
			$query->where('user_id', $this->user->id);
		}
	}

	/*
	 * Reorder the row positions
	 */
	public function index_onUpdatePosition() {

		$model = $this->_getModel();

		$moved = [];
		$position = 1;
		if (($reorderIds = post('checked')) && is_array($reorderIds) && count($reorderIds)) {
			foreach ($reorderIds as $id) {

				if (in_array($id, $moved) || !$record = $model->find($id)) {
					continue;
				}

				$record->sort_order = $position;
				$record->update();
				$moved[] = $id;
				$position++;
			}
			Flash::success('Itens reordenados!');
		}
		return $this->listRefresh();
	}

	public function index_onDelete() {

		$model = $this->_getModel();

		if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

			foreach ($checkedIds as $postId) {
				if (!$item = $model->find($postId)) {
					continue;
				}

				$item->delete();
			}

			Flash::success('Item(s) deletados com sucesso!');
		}

		return $this->listRefresh();
	}

	protected function _getModel() {
		$class = explode('\\', get_class($this));
		$context = array_pop($class);
		array_pop($class);
		$namespace = array_pop($class);
		$model = 'EveraldoReis\\' . $namespace . '\Models\\' . str_singular($context);
		return new $model;
	}

	function onUpdateSlug() {

		$prefix = post('prefix');
		$suffix = post('suffix');

		if (empty($prefix)) {
			return ['slug' => '/' . Str::slug($suffix)];
		}

		$prefix = Categoria::find($prefix)->slug;
		$suffix = Str::slug($suffix);

		return ['slug' => $prefix . '/' . $suffix];
	}

}