<?php namespace EveraldoReis\Posts\Controllers;

use Backend\Classes\Controller;

/**
 * Categorias Back-end Controller
 */
class Categorias extends PluginController {
	public $sortable = true;
}