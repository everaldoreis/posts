<?php namespace EveraldoReis\Posts;

use Backend;
use BackendMenu;
use Str;
use System\Classes\PluginBase;

/**
 * Posts Plugin Information File
 */
class Plugin extends PluginBase {

	/**
	 * Returns information about this plugin.
	 *
	 * @return array
	 */
	public function pluginDetails() {
		return [
			'name' => 'Posts',
			'description' => 'No description provided yet...',
			'author' => 'EveraldoReis',
			'icon' => 'icon-leaf',
		];
	}

	public function registerComponents() {
		return [
			'EveraldoReis\Posts\Components\Categoria' => 'Categorias',
			'EveraldoReis\Posts\Components\Carousel' => 'Carousel',
		];
	}

	public function registerMailTemplates() {
		return [];
	}

	public function registerMarkupTags() {
		return [
			'filters' => [
				'slugify' => function ($str) {
					return Str::slug($str);
				},
			],
		];
	}

	public function registerPermissions() {
		return [];
	}

	public function register() {
		BackendMenu::registerContextSidenavPartial('EveraldoReis.Posts', 'posts', '@/plugins/everaldoreis/posts/partials/_sidebar.htm');
	}

	public function registerNavigation() {
		return [
			'posts' => [
				'label' => 'Posts',
				'icon' => '',
				'url' => Backend::url('everaldoreis/posts/posts'),
				'permissions' => ['everaldoreis.posts.*'],
				'sideMenu' => [
					'categoriaindex' => [
						'group' => 'Posts',
						'label' => 'Categorias',
						'icon' => '',
						'url' => Backend::url('everaldoreis/posts/categorias'),
						'permissions' => ['everaldoreis.posts.categoriaindex'],
					],
					'categoriacreate' => [
						'group' => 'Posts',
						'label' => 'Nova Categoria',
						'icon' => '',
						'url' => Backend::url('everaldoreis/posts/categorias/create'),
						'permissions' => ['everaldoreis.posts.categoriacreate'],
					],
					'postsindex' => [
						'group' => 'Posts',
						'label' => 'Posts',
						'icon' => '',
						'url' => Backend::url('everaldoreis/posts/posts'),
						'permissions' => ['everaldoreis.posts.postsindex'],
					],
					'postscreate' => [
						'group' => 'Posts',
						'label' => 'Novo Post',
						'icon' => '',
						'url' => Backend::url('everaldoreis/posts/posts/create'),
						'permissions' => ['everaldoreis.posts.postscreate'],
					],
				],
			],
		];
	}
}
