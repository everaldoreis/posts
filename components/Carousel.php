<?php namespace EveraldoReis\Posts\Components;

use Cms\Classes\ComponentBase;

class Carousel extends ComponentBase {

	public function componentDetails() {

		return [
			'name' => 'Carousel Component',
			'description' => 'No description provided yet...',
		];

	}

	public function defineProperties() {

		return [
			'carousel' => [
				'title' => 'Carousel',
				'type' => 'dropdown',
				'required' => false,
				'emptyOption' => '0',
			],
		];

	}

	public function post() {

		$url = '/' . Request::path();

		$post = ModelPost::whereSlug($url)->first();

		return $post;

	}

}