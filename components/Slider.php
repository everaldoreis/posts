<?php namespace EveraldoReis\Posts\Components;

use Cms\Classes\ComponentBase;

class Slider extends ComponentBase {

	public function componentDetails() {
		return [
			'name' => 'Slider Component',
			'description' => 'No description provided yet...',
		];
	}

	public function defineProperties() {
		return [];
	}

}