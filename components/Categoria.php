<?php namespace EveraldoReis\Posts\Components;

use Cms\Classes\ComponentBase;
use EveraldoReis\Posts\Models\Categoria as ModelCategoria;
use EveraldoReis\Posts\Models\Post as ModelPost;
use Request;

class Categoria extends ComponentBase {

	public function componentDetails() {
		return [
			'name' => 'Categoria Component',
			'description' => 'No description provided yet...',
		];
	}

	public function defineProperties() {
		return [
			'categoria' => [
				'title' => 'Categoria',
				'type' => 'dropdown',
				'required' => false,
				'emptyOption' => '0',
			],
			'per_page' => [
				'title' => 'Notícias por página',
				'type' => 'string',
				'required' => false,
				'default' => 1,
			],
			'exibir_subcategorias' => [
				'title' => 'Exibir subcategorias',
				'type' => 'checkbox',
				'default' => 1,
				'required' => false,
			],
			'exibir_posts' => [
				'title' => 'Exibir posts',
				'type' => 'checkbox',
				'default' => 1,
				'required' => false,
			],
			'mostrar_data' => [
				'title' => 'Exibir data',
				'type' => 'checkbox',
				'default' => 0,
				'required' => false,
			],
			'show_link' => [
				'title' => 'Exibir link',
				'type' => 'checkbox',
				'default' => 0,
				'required' => false,
			],
			'layout' => [
				'title' => 'Layout',
				'type' => 'dropdown',
				'default' => 'default',
				'required' => true,
				'options' => [
					'default' => 'Default',
					'cards' => 'Cards',
				],
			],
		];
	}

	public function exibir_posts() {
		return $this->property('exibir_posts');
	}

	public function exibir_subcategorias() {
		return $this->property('exibir_subcategorias');
	}

	public function post() {
		$url = '/' . Request::path();

		$post = ModelPost::whereSlug($url)->first();

		return $post;

	}

	public function categoria() {
		$url = '/' . Request::path();

		return ModelCategoria::whereSlug($url)->first();

	}

	public function categorias() {

		$categorias = ModelCategoria::wherePublished(1);

		if ($categoria = $this->categoria()) {
			$categorias->whereParentId($categoria->id);
		} else {
			if ($categoria = $this->property('categoria')) {
				$categorias->whereParentId($categoria);
			}
		}

		return $categorias->get();

	}

	public function posts() {

		$posts = ModelPost::wherePublished(1);

		if ($categoria = $this->categoria()) {
			$posts->whereCategoriaId($categoria->id);
		} else {
			if ($categoria = $this->property('categoria')) {
				$posts->whereCategoriaId($categoria);
			}
		}

		return $posts
			->skip($this->property('per_page', 12) * (Request::input('page', 0) - 1))
			->take($this->property('per_page', 12))
			->get();

	}

	public function pagination() {

		$posts = ModelPost::wherePublished(1);

		if ($categoria = $this->categoria()) {
			$posts->whereCategoriaId($categoria->id);
		} else {
			if ($categoria = $this->property('categoria')) {
				$posts->whereCategoriaId($categoria);
			}
		}

		return ceil($posts->count() / $this->property('per_page', 12));

	}

	public function getCategoriaOptions() {

		$categorias = ModelCategoria::wherePublished(1)->get();

		$items = [0 => 'Todas'];

		foreach ($categorias as $categoria) {
			$items[$categoria->id] = $categoria->name;
		}

		return $items;

	}

	public function onRun() {
		$this->addJs('/plugins/everaldoreis/posts/assets/js/main.js');
		$this->addCss('/plugins/everaldoreis/posts/assets/css/main.css');
		$this->page['current_url'] = Request::path();
		$this->page['curr_page'] = Request::input('page', 1);
		$this->page['pageLayout'] = $this->property('layout', 'default');
		$this->page['mostrar_data'] = $this->property('mostrar_data', 0);
	}

}