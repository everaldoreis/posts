<?php

use EveraldoReis\Posts\Models\Categoria;
use EveraldoReis\Posts\Models\Post;
use Illuminate\Support\Str;

// Faz autenticação do cliente
Route::filter('auth', function () {
	// tokens permitidos
	$tokens = ['123456123456'];

	if (!Request::input('access_token')) {
		return 'Access token is required';
	}

	if (!in_array(Request::input('access_token'), $tokens)) {
		return 'Invalid access token';
	}

});

// Grupo de rotas para saidas JSON
Route::group(['prefix' => 'posts', 'before' => 'auth'], function ($id) {

	// List posts
	Route::get('/', function () {
		return Post::wherePublished(1)->get();
	});

	// Get single post
	Route::get('/{id}', function ($id) {
		return Post::wherePublished(1)->find($id);
	});

	// Create post
	Route::post('/', function ($id) {
		$post = new Post;
		$post->name = Request::input('name');
		$post->description = Request::input('description');
		$post->categoria = Categoria::wherePublished(1)->find(Request::input('categoria_id'));
		$post->published = Request::input('published');
		$post->slug = $post->categoria->slug . '/' . Str::slug($post->name);
		$post->save();
		return $post;
	});

	// Update post
	Route::put('/{id}', function ($id) {
		$post = Post::wherePublished(1)->find($id);
		$post->name = Request::input('name', $post->name);
		$post->description = Request::input('description', $post->description);
		$post->categoria = Categoria::wherePublished(1)->find(Request::input('categoria_id', $post->categoria->id));
		$post->published = Request::input('published', $post->published);
		$post->slug = $post->categoria->slug . '/' . Str::slug($post->name);
		$post->update();
		return $post;
	});

});