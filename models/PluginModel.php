<?php namespace EveraldoReis\Posts\Models;

use Model;

/**
 * PluginModel Model
 */
class PluginModel extends Model {

	use \October\Rain\Database\Traits\NestedTree;
	use \October\Rain\Database\Traits\Sortable;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = '';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = [];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [];
	public $attachMany = [];

}