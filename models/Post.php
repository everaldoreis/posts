<?php namespace EveraldoReis\Posts\Models;

use Model;

/**
 * Post Model
 */
class Post extends PluginModel {

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'everaldoreis_posts_posts';

	public $belongsTo = ['categoria' => 'EveraldoReis\Posts\Models\Categoria'];

	public $attachOne = ['image' => 'System\Models\File'];

	public $attachMany = ['gallery' => 'System\Models\File'];

	//return a collection of categories
	public function breadcrumbs() {

		$collection = [];

		$item = $this;

		while ($item->category->parent) {
			$item = $item->category->parent;
			$collection[] = $item;
		}

		return array_reverse($collection);

	}

}