<?php namespace EveraldoReis\Posts\Models;

use Model;

/**
 * Categoria Model
 */
class Categoria extends PluginModel {

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'everaldoreis_posts_categorias';

	public $belongsTo = ['parent' => 'EveraldoReis\Posts\Models\Categoria'];

	public $attachOne = ['image' => 'System\Models\File'];

	//return a collection of categories
	public function breadcrumbs() {

		$collection = [];

		$item = $this;

		while ($item->parent) {
			$item = $item->parent;
			$collection[] = $item;
		}

		return array_reverse($collection);

	}

}